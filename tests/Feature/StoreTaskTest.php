<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Http\Response;

class StoreTaskTest extends AbstractApiTestCase
{
    const TASK_STORE_PATH = '/api/tasks';

    /**
     * @test
     * @dataProvider createWithWrongDataProvider
     *
     * @param $data
     * @param $expected
     */
    public function create_new_task_with_wrong_data($data, $expected)
    {
        $this->apiPost(self::TASK_STORE_PATH, $data)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors($expected);
    }

    /**
     * @return array
     */
    public function createWithWrongDataProvider()
    {
        return [
            [
                [],
                ['name', 'started_at', 'finished_at']
            ],
            [
                ['name' => 'Task Name', 'started_at' => 'Not a valid date', 'finished_at' => Carbon::tomorrow()->toDateTimeString()],
                ['started_at']
            ],
            [
                ['name' => 'Task Name', 'started_at' => Carbon::today()->format('Y-m-d 18:00:00'), 'finished_at' => Carbon::tomorrow()->format('Y-m-d 19:13:00')],
                ['finished_at']
            ],
        ];
    }
}
