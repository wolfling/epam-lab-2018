<?php

namespace Tests\Feature;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class TasksTest extends AbstractApiTestCase
{
    use RefreshDatabase;

    const TASKS_PATH = '/api/tasks';

    /**
     * @test
     */
    public function tasks_are_well_structured()
    {
        factory(Task::class, 1)->create();

        $this->apiGet(self::TASKS_PATH)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(self::TASKS_STRUCTURE);
    }

    /**
     * @test
     */
    public function tasks_for_specific_date()
    {
        factory(Task::class, 3)->create([
            'started_at' => Carbon::tomorrow()->toDateTimeString(),
        ]);
        factory(Task::class, 2)->create([
            'started_at' => Carbon::today()->toDateTimeString(),
        ]);

        $data = [
            'started_at' => Carbon::tomorrow()->format('Y-m-d'),
        ];

        $response = $this->apiGet(self::TASKS_PATH, $data)->decodeResponseJson();

        $this->assertEquals(3, sizeof($response['data']));
    }
}
