<?php

namespace Tests\Feature;

use Tests\TestCase;

abstract class AbstractApiTestCase extends TestCase
{
    const NOTIFICATION_STRUCTURE = [
        'id',
        'type',
        'alarmed_at',
    ];

    const TASK_STRUCTURE = [
        'id',
        'name',
        'description',
        'started_at',
        'finished_at',
        'notifications' => [
            '*' => self::NOTIFICATION_STRUCTURE
        ]
    ];

    const TASKS_STRUCTURE = [
        'data' => [
            0 => self::TASK_STRUCTURE,
        ]
    ];

    /**
     * @param string $path
     * @param array $data
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function apiPost($path, $data)
    {
        return $this->json('POST', $path, $data, $this->getHeaders());
    }

    /**
     * @param $path
     * @param $data
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function apiGet($path, $data = [])
    {
        return $this->getJson(sprintf('%s?%s', $path, http_build_query($data)), $this->getHeaders());
    }

    /**
     * Returns API headers like Auth etc
     *
     * @return array
     */
    protected function getHeaders()
    {
        return [];
    }
}