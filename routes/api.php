<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('api')->get('/tasks', function (Request $request) {
//    return [];
//});
//
//Route::middleware('api')->post('/tasks', function (\App\Http\Requests\StoreTaskRequest $request) {
//    return [];
//});

Route::apiResource('tasks', 'TaskController');