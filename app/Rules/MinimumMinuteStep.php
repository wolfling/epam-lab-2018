<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class MinimumMinuteStep implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    const MINIMUM_MINUTE_STEP = 5;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return ($date->minute % self::MINIMUM_MINUTE_STEP) == 0;
        } catch (\InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Minimum Minutes step MUST be equal 5 minutes';
    }
}
