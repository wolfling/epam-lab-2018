<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const TYPE_SMS = 'sms';
    const TYPE_EMAIL = 'email';

    protected $fillable = [
        'task_id', 'type', 'alarmed_at',
    ];

    protected $casts = [
        'alarmed_at' => 'datetime',
    ];

    /**
     * Notification belongs to Task
     */
    public function airport()
    {
        return $this->belongsTo(Task::class);
    }
}
