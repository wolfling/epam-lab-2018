<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use Filterable;

    protected $fillable = [
        'name', 'description', 'started_at', 'finished_at',
    ];

    protected $casts = [
        'started_at' => 'datetime',
        'finished_at' => 'datetime',
    ];

    /**
     * Tasks has many notifications
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
}
