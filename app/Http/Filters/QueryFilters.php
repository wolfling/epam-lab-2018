<?php

namespace App\Http\Filters;

/**
 * Base class for all filters to extend
 */

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

abstract class QueryFilters
{
    protected $request;
    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Returns all filters
     *
     * @return array
     */
    public function filters()
    {
        return $this->request->all();
    }

    /**
     * Applies filters to Query Builder
     *
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;

        foreach ($this->filters() as $name => $value) {
            if ($value == 'all' || !method_exists($this, $name)) {
                continue;
            }

            call_user_func_array([$this, $name], [$value]);
        }

        return $this->builder;
    }
}