<?php

namespace App\Http\Filters;

use Carbon\Carbon;

class TaskFilters extends QueryFilters
{
    public function started_at($date)
    {
        if (Carbon::createFromFormat('Y-m-d', $date) !== false) {
            $this->builder->whereBetween('started_at', [
                sprintf('%s 00:00:00', $date),
                sprintf('%s 23:59:59', $date)
            ]);
        }
    }
}
