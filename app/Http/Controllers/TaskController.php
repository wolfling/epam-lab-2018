<?php

namespace App\Http\Controllers;

use App\Http\Filters\TaskFilters;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Resources\TasksResource;
use App\Models\Task;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TaskController extends Controller
{
    const TASKS_PER_PAGE = 15;

    /**
     * Display a listing of the resource.
     *
     * @param TaskFilters $filters
     *
     * @return TasksResource|AnonymousResourceCollection
     */
    public function index(TaskFilters $filters)
    {
        return TasksResource::collection(
            Task::with('notifications')
                ->filter($filters)
                ->paginate(self::TASKS_PER_PAGE)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTaskRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreTaskRequest  $request
     * @param  Task  $task
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTaskRequest $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Task  $task
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
