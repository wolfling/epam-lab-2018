<?php

use Illuminate\Database\Seeder;
use App\Models\Notification;
use App\Models\Task;
use Carbon\Carbon;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks = Task::all();

        foreach ($tasks as $task) {
            /** @var Carbon $alarmedAt */
            $alarmedAt = $task->started_at;

            factory(Notification::class)->create([
                'task_id' => $task->id,
                'alarmed_at' => $alarmedAt->subMinute(15),
            ]);
        }
    }
}
