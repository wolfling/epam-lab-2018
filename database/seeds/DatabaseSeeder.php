<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateAffectedTables();

        $this->call(TasksTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
    }

    /**
     * Function return tables which are affected by this seed
     *
     * @return array
     */
    protected function getTables(): array
    {
        return [
            'tasks',
            'notifications'
        ];
    }

    /**
     * Truncates all the tables affected this seeder
     *
     * @return void
     */
    protected function truncateAffectedTables()
    {
        DB::statement('SET foreign_key_checks = 0');

        foreach ($this->getTables() as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET foreign_key_checks = 1');
    }
}
