<?php

use Faker\Generator as Faker;
use App\Models\Task;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'name' => $faker->text(),
        'description' => $faker->realText(),
        'started_at' => $faker->dateTimeThisMonth->format('Y-m-d H:00:00'),
        'finished_at' => $faker->dateTimeThisMonth->format('Y-m-d H:00:00'),
    ];
});
