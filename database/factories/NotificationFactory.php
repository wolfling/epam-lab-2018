<?php

use Faker\Generator as Faker;
use App\Models\Notification;

$factory->define(Notification::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement([Notification::TYPE_SMS, Notification::TYPE_EMAIL]),
        'task_id' => $faker->numberBetween(1, 100),
        'alarmed_at' => $faker->dateTimeThisMonth->format('Y-m-d H:00:00'),
    ];
});
